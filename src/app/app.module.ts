import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MobHomeComponent } from './components/mob-home/mob-home.component';
import { HttpClientModule } from '@angular/common/http';
import { CityDetailComponent } from './components/mob-city-detail/mob-city-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    MobHomeComponent,
    CityDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
