import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MobWeatherService {

  constructor(private http: HttpClient) { }

  fetchWeather(cityCode) {
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${cityCode}&appid=3d8b309701a13f65b660fa2c64cdc517`;
    return this.http.get(url);
  }

  fetchWeatherForecast(cityCode) {
    const url = `http://api.openweathermap.org/data/2.5/forecast?q=${cityCode}&cnt=40&appid=3d8b309701a13f65b660fa2c64cdc517`;
    return this.http.get(url);
  }
}
