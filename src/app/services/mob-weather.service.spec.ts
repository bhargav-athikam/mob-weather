import { TestBed } from '@angular/core/testing';

import { MobWeatherService } from './mob-weather.service';

describe('MobWeatherService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));
  const httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);

  it('should be created', () => {
    const service: MobWeatherService = new MobWeatherService(httpClientMock);
    expect(service).toBeTruthy();
  });
});
