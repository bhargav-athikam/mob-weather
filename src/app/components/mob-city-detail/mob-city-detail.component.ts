import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MobWeatherService } from 'src/app/services/mob-weather.service';

@Component({
  selector: 'app-city-detail',
  templateUrl: './mob-city-detail.component.html',
  styleUrls: ['./mob-city-detail.component.scss']
})
export class CityDetailComponent implements OnInit {
  cityName: string;
  cityWeatherForecastResponse: any = [];
  showErrorMsg: string;
  constructor(private route: ActivatedRoute, private mobWeatherService: MobWeatherService) { }

  ngOnInit() {
    this.cityName = this.route.snapshot.paramMap.get('cityname');
    this.getForecastByCity();
  }

  private getForecastByCity() {
    this.mobWeatherService.fetchWeatherForecast(this.cityName)
    .subscribe((resp:any) => {
      if(!resp) return;
      resp.list.forEach(element => {
        const date_txt = element.dt_txt.split(" ");
        if(date_txt[1] === '09:00:00') {
          const dataToPush = { 
            temperature: element.main.temp,
            seaLevel: element.main.sea_level,
            date: date_txt[0]
           }
           this.cityWeatherForecastResponse.push(dataToPush); 
        }
      });
    }, (error) => {
      this.showErrorMsg = error.message;
    });
  }

}
