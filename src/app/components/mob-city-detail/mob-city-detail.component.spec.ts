import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityDetailComponent } from './mob-city-detail.component';
import { MobWeatherService } from 'src/app/services/mob-weather.service';
import { ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
const mockResp = {
  list: [
    {
      dt_txt: "2020-10-1 09:00:00",
      main: {
        temp: 12,
        sea_level: 123123
      }
    },
    {
      dt_txt: "2020-10-1 06:00:00",
      main: {
        temp: 11,
        sea_level: 123123
      }
    },
    {
      dt_txt: "2020-10-2 09:00:00",
      main: {
        temp: 11,
        sea_level: 123123
      }
    }
  ]
}

describe('CityDetailComponent', () => {
  let component: CityDetailComponent;
  let fixture: ComponentFixture<CityDetailComponent>;
  let mobWeatherServiceMock = jasmine.createSpyObj('MobWeatherService', ['fetchWeatherForecast']);
  let activatedRouteMock = jasmine.createSpyObj('ActivatedRoute', ['snapshot']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityDetailComponent ],
        providers: [
          { provide: MobWeatherService, useValue: mobWeatherServiceMock },
          { provide: ActivatedRoute, useValue: activatedRouteMock }
        ]
    })
    .compileComponents();
  }));

  
  beforeEach(() => {
    fixture = TestBed.createComponent(CityDetailComponent);
    component = fixture.componentInstance;
    activatedRouteMock.snapshot.paramMap = {
      get: () => 'london'
    }
    mobWeatherServiceMock.fetchWeatherForecast.and.returnValue(of(mockResp));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have city name', () => {
    expect(component.cityName).toBe('london');
  });

  it('should have cityWeatherForecastResponse', () => {
    expect(component.cityWeatherForecastResponse).toEqual([{temperature: 12, seaLevel: 123123, date: '2020-10-1'}, {temperature: 11, seaLevel: 123123, date: '2020-10-2'}]);
  });

});
