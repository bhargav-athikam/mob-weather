import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { MobHomeComponent } from './mob-home.component';
import { MobWeatherService } from 'src/app/services/mob-weather.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('MobHomeComponent', () => {
  let component: MobHomeComponent;
  let fixture: ComponentFixture<MobHomeComponent>;
  let mobWeatherServiceMock = jasmine.createSpyObj('MobWeatherService', ['fetchWeather']);
  let routerMock = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobHomeComponent ],
      providers: [
        { provide: MobWeatherService, useValue: mobWeatherServiceMock },
        { provide: Router, useValue: routerMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobHomeComponent);
    component = fixture.componentInstance;
    mobWeatherServiceMock.fetchWeather.and.returnValue(of({main:{temp:1},sys:{ sunrise:213, sunset:2132 }}))
    fixture.detectChanges();
   
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have cityWeatherResponse', () => {
    expect(component.cityWeatherResponse.length).toBe(5);
  });

  it('should have cityWeatherResponse temperature to be 1', () => {
    expect(component.cityWeatherResponse[0].temp).toBe(1);
  });
  
  it('should call router navigate on click gotoMoreCityDetails', () => {
    component.gotoMoreCityDetails({cityName: 'london'});
    expect(routerMock.navigate).toHaveBeenCalledWith(['/city', 'london']);
  });

  
});
