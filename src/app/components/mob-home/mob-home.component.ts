import { Component, OnInit } from '@angular/core';
import { MobWeatherService } from 'src/app/services/mob-weather.service';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-mob-home',
  templateUrl: './mob-home.component.html',
  styleUrls: ['./mob-home.component.scss']
})
export class MobHomeComponent implements OnInit {
  cityList:Array<string> = ['paris', 'london', 'madrid', 'munich', 'milan'];
  cityWeatherResponse: any = [];

  constructor(private mobWeatherService: MobWeatherService, private router: Router) { }
  

  ngOnInit() {
    this.getWeatherData();
  }

  private getWeatherData() {
    const urlList = [];
    this.cityList.forEach(element => {
      urlList.push(this.mobWeatherService.fetchWeather(element));
    });

    forkJoin(urlList)
    .subscribe((resp) => {
      this.cityWeatherResponse = resp.map((item) => {
        return {
          temp: item.main.temp,
          sunrise: moment(item.sys.sunrise).format('LLL'),
          sunset: moment(item.sys.sunset).format('LLL'),
          cityName: item.name
        }
      })
    }, (error) => {
      console.log("error")
    });
  }

  gotoMoreCityDetails(city) {
    this.router.navigate(['/city', city.cityName]);
  }
}
