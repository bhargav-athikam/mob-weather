import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MobHomeComponent } from './components/mob-home/mob-home.component';
import { CityDetailComponent } from './components/mob-city-detail/mob-city-detail.component';


const routes: Routes = [
{path: 'home', component: MobHomeComponent},
{path: 'city/:cityname', component: CityDetailComponent},
{path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
